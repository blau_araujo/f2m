# Fuzzy File Manager (f2m.sh)

Um gerenciador de arquivos "suckless" didático para terminal e ambientes gráficos.

## Módulos básicos

  * [ ] Navegar entre as pastas
  * [ ] Bookmarks (pastas)
  * [ ] Raiz do sistema (navegação)
  * [ ] Abrir pasta no terminal
  * [ ] Abrir pasta no FM gráfico
  * [ ] Abrir arquivos de tipos conhecidos
  * [ ] Abrir com...
  * [ ] Histórico
  * [ ] Busca de pastas e arquivos

## Módulos de gerenciamento

  * [ ] Abrir como root
  * [ ] Seleção múltipla
  * [ ] Criar pastas e arquivos (inc. a partir de modelos)
  * [ ] Mover pastas e arquivos
  * [ ] Renomear pastas e arquivos
  * [ ] Mover pastas e arquivos para a lixeira
  * [ ] Compactar arquivos (tar, zip e rar)
  * [ ] Descompactar arquivos

## Módulo de configuração (~/.config/f2m/config)

  * [ ] Preferências (basicamente pastas)
  * [ ] Bookmarks
  * [ ] Aplicativos (abrir com, compactar, etc...)
  * [ ] Fontes e cores (dmenu)

## Decisões do projeto

  * Usar lib externa ao script?
  * Menu de ações apenas após seleção?
  * Como lidar com a opção de exibição de arquivos ocultos?
  * Como lidar com as pastas pai (..)
  * Ao sair, eliminar opções marcadas pendentes?
  * Teremos uma opção "Desfazer"?
  * O menu sempre abre na HOME?
  * Que tal navegar máquinas remotas via SSH?
